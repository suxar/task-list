var tasks = JSON.parse(localStorage.getItem('tasks'));
if (!tasks) {
    tasks = [];
}

$('#dates').daterangepicker({
    timePicker: true,
    singleDatePicker: true,
    timePicker24Hour: true,
    locale: {
        format: 'DD/MM/YYYY HH:mm'
    }
});

const container = document.getElementsByClassName('container')[0];
const modalWrapper = document.getElementsByClassName('modal-wrapper')[0];
modalWrapper.addEventListener('click', hideDeletemodal);
document.getElementsByClassName('confirm-modal')[0].addEventListener('click', stopProp);
document.getElementsByClassName('add-modal')[0].addEventListener('click', stopProp);
document.getElementsByClassName('close-modal')[0].addEventListener('click', hideDeletemodal);
document.getElementsByClassName('close-modal')[1].addEventListener('click', hideDeletemodal);
document.getElementsByClassName('add-task')[0].addEventListener('click', ShowAddModal);
document.getElementById('submit').addEventListener('click', AddTask);


const modalButtons = document.getElementsByClassName('item');
for (var i = 0; i < modalButtons.length; i++) {
    modalButtons[i].addEventListener('click', hideDeletemodal);
}

document.getElementById('confirm').addEventListener('click', deleteTask);

var taskId;

renderAll();

const checkbox = document.getElementsByClassName('checkbox');
for (var i = 0; i < checkbox.length; i++) {
    checkbox[i].addEventListener('click', checkCheckbox);
}

const Del = document.getElementsByClassName('task-delete');
for (var i = 0; i < Del.length; i++) {
    Del[i].addEventListener('click', ShowModalDelete);
}


function renderTask(tasks) {
    container.innerHTML += `
    <div class="task">

        <div class="task-info">
            <input type="checkbox" name="" class="checkbox" id="${tasks.id}" ${tasks.is_complete ? 'checked' : ''}>
            <span class="task-deadline">${convertToDate(tasks.deadline)}</span>
        </div>
        <span class="task-name">${tasks.title}</span>

        <div class="task-delete" id="${tasks.id}">
            <i class="far fa-times-circle"></i>
        </div>

    </div>
    `;
}

function renderAll() {
    container.innerHTML = '';
    tasks.forEach(item => renderTask(item));
}

function convertToDate(time) {
    moment.locale('ru');
    return moment.unix(time).format('lll');
}

function checkCheckbox() {
    checkId = this.id;
    console.log(this);
    var res = this.hasAttribute(checked);
    console.log(res);
 //  if( this.hasAttribute(checked) == true ){
 //      console.log(1);

 //     //wrapper.container.task.task - info.checkbox.style.setAttribute('style', 'text-decoration:line-through'); 
 //  } else {
 //      console.log(2);
 //  }
    
}

function ShowModalDelete() {
    taskId = this.id;
    modalWrapper.style.display = 'flex';
    document.getElementsByClassName('confirm-modal')[0].style.display = 'flex';
    document.getElementsByClassName('add-modal')[0].style.display = 'none';
}

function ShowAddModal() {
    modalWrapper.style.display = 'flex';
    document.getElementsByClassName('confirm-modal')[0].style.display = 'none';
    document.getElementsByClassName('add-modal')[0].style.display = 'flex';
}

function hideDeletemodal() {
    modalWrapper.style.display = 'none';
}

function stopProp(event) {
    event.stopPropagation();
}

function deleteTask(id) {
    const task = tasks.find(item => item.id === +taskId);
    const index = tasks.indexOf(task);
    tasks.splice(index, 1);
    renderAll();
    localStorage.setItem('tasks', JSON.stringify(tasks));
    const Del = document.getElementsByClassName('task-delete');
    for (var i = 0; i < Del.length; i++) {
        Del[i].addEventListener('click', ShowModalDelete);
    }
    const checkbox = document.getElementsByClassName('checkbox');
    for (var i = 0; i < checkbox.length; i++) {
        checkbox[i].addEventListener('click', checkCheckbox);
    }
}

function AddTask() {
    var drp = $('#dates').val();
    const unix = moment(drp, 'DD/MM/YYYY HH:mm').unix();
    var maxId = 0;
    for (var i = 0; i < tasks.length; i++) {
        if (maxId < tasks[i].id) {
            maxId = tasks[i].id;
        }
    }
    const taskname = document.getElementById('title').value;
    const newId = maxId + 1;
    const task = {
        id: newId,
        title: taskname,
        deadline: unix,
        is_complete: false
    };
    tasks.push(task);
    sortTasks();
    renderAll();
    localStorage.setItem('tasks', JSON.stringify(tasks));
    hideDeletemodal();
    const Del = document.getElementsByClassName('task-delete');
    for (var i = 0; i < Del.length; i++) {
        Del[i].addEventListener('click', ShowModalDelete);
    }
    const checkbox = document.getElementsByClassName('checkbox');
    for (var i = 0; i < checkbox.length; i++) {
        checkbox[i].addEventListener('click', checkCheckbox);
    }
}

function sortTasks() {
    tasks.sort((a, b) => {
        return a.deadline - b.deadline;
    })
}